'use strict'

class Country{
    constructor(name, flag, capital, region, currencies) {
        this.name = name;
        this.flag = flag;
        this.capital = capital;
        
        this.region = region;
        this.currencies = currencies;
        
    }
}

function addCountry(element) {
    document.getElementById("countryBlock").appendChild(element);
    // element.insertBefore(element,  document.getElementById("countryBlock"))
}
// function addCountry(element) {
//     let y = document.getElementById("countryBlock");
//     y.insertBefore(element, y.childNodes[2]);
// }
// 


function createCountry(country) {
    let element = document.createElement(`div`);
    element.innerHTML = `
    <h3>${country.name}</h3>
    <img class="d-block w-100 img" src="${country.flag}">
    <p>Capital: ${country.capital}</p>
    <p>Region: ${country.region}</p>
    <p>Currencies: ${country.currencies}</p>
    <a href="https://www.google.com/search?q=${country.name}" target="_blank">More in goole</a>
    <p></p>
    <a href="https://ru.m.wikipedia.org/wiki/${country.name}" target="_blank">More in wikipedia</a>`;
    let newClass = document.createAttribute("class");
    newClass.value = "card my-3 text-white bg-dark";
    element.setAttributeNode(newClass);
    return element;
}


async function findCountry() {
    const myForm = document.getElementById("my-form");
    let data = new FormData(myForm);
    let name = data.get("country");
    const response = await fetch('https://restcountries.eu/rest/v2/name/'+name);
    if (response.ok) {
        let countryJson = await response.json();
        let country = new Country(
        countryJson[0].name,
        countryJson[0].flag,
        countryJson[0].capital,
    
        countryJson[0].region,
        countryJson[0].currencies[0].name,
        );
        let element = createCountry(country);
        addCountry(element);

        document.getElementById("sss").focus();
    }
    else{
        alert("So country doesn't exist");

        document.getElementById("sss").focus();
    }
}